package com.isoft.huffmanencoding;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class HuffmanEncode {
	HashMap<Character, String> codeTable = new HashMap<Character, String>();
	PriorityQueue<Node> binaryTree = new PriorityQueue<Node>();
	Node root;
	
	//Builds the Huffman binary tree.
	public Node buildHuffmanTree(HashMap<Character, Integer> lettersFreq) {
		//Create nodes for each character.
		for(Map.Entry<Character, Integer> entry : lettersFreq.entrySet()) {
			binaryTree.add(new Node(entry.getKey(),entry.getValue()));	
		}
		//Get the least occurring 2 nodes and combine them and add left and right nodes. Do this until there is only one node left.
		while(binaryTree.size() > 1) {
			Node first = binaryTree.poll();
			Node second = binaryTree.poll();
			Node combined = new Node('\0',first.getFrequency() + second.getFrequency(),first,second);
			binaryTree.add(combined);
		}
		
		//One remaining node is the root
		root = binaryTree.poll();
		return root;
	}
	
	//Traverse the binary tree. 
	//If the node is left add 0 to the code, if it is right add 1, if the node is a leaf stop. Finally store them in a HashMap.
	public void buildCodeTable(HashMap<Character,String> codeTable,Node root, String code){
	    if(root.getLeft() != null)
	    	buildCodeTable(codeTable,root.getLeft(), code + "0");
	    if(root.getRight() != null)
	    	buildCodeTable(codeTable,root.getRight(),code + "1");
	    codeTable.put(root.getCharacter(),code);
	}
	
	public HashMap<Character,String> getCodes() {
		HashMap <Character,String> codeTable = new HashMap<Character,String>();
		buildCodeTable(codeTable, root.getLeft(), "0");
        buildCodeTable(codeTable, root.getRight(), "1");
		return codeTable;
	}
	//Encode the string.
	public String encode(HashMap<Character,String> codeTable,String text) {
		StringBuilder encodedString = new StringBuilder();
		for(char ch : text.toCharArray()) {
			encodedString.append(codeTable.get(ch));
		}
		return encodedString.toString();
	}
	
	public int getHeight(Node root) {
		
		int leftHeight = 0, rightHeight = 0;
		
		if(root.getLeft() != null) {
			leftHeight = getHeight(root.getLeft());
			leftHeight++;
		}
		if(root.getRight() != null) {
			rightHeight = getHeight(root.getRight());
			rightHeight++;
		}
		
		int max = (leftHeight > rightHeight ? leftHeight : rightHeight);
		return max;
	}

	
	
}
