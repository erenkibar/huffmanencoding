package com.isoft.huffmanencoding;

import java.util.HashMap;

public class FrequencyCalculator {

	private HashMap<Character, Integer> freq= new HashMap<Character, Integer>();
	

	public HashMap<Character, Integer> getFreq() {
		return freq;
	}

	public void setFreq(HashMap<Character, Integer> freq) {
		this.freq = freq;
	}
	
	//Calculates the occurance of each char in a string.
	public HashMap<Character, Integer> calculateOccurance(String text) {
		char[] strArray = text.toCharArray();
		for(char ch : strArray) {
			//If char is already in the hash map increase its count, if not set it to 1. 
			if(freq.containsKey(ch)) {
				freq.put(ch, freq.get(ch) + 1);
			} else {
				freq.put(ch,1);
			}	
		}
		return freq;
	}

	@Override
	public String toString() {
		return "FrequencyCalculator [freq=" + freq + "]";
	}

}
