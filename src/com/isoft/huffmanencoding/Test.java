package com.isoft.huffmanencoding;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.PriorityQueue;

public class Test {
	static HashMap<Character, Integer> lettersFreq = new HashMap<Character, Integer>();
	static PriorityQueue<Node> binaryTree = new PriorityQueue<Node>();

	static String text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
			+ "Nunc vehicula, risus accumsan consequat bibendum, orci nisl dapibus dolor, ac pretium quam sapien in ipsum."
			+ "Nam rhoncus et diam sodales hendrerit. Etiam imperdiet tempor porta."
			+ "Etiam sit amet ante non ipsum sagittis porta."
			+ "Sed sagittis ultricies egestas."
			+ "Sed id posuere eros, rutrum pharetra arcu. Cras suscipit dolor nec est faucibus consectetur.";
	
	static String lowercase = text.toLowerCase();
	
	public static void main(String[] args) throws UnsupportedEncodingException {
		//Calculate frequencies of letters.
		FrequencyCalculator freqCalc = new FrequencyCalculator();
		lettersFreq = freqCalc.calculateOccurance(lowercase);
		//Build the Huffman tree.
		HuffmanEncode huffman = new HuffmanEncode();
		Node root = huffman.buildHuffmanTree(lettersFreq);
		//Build the code map.
		System.out.printf("Previous length of string: %s\n", lowercase.getBytes("ASCII").length * 8);
		//Encode the string.
		String encoded = huffman.encode(huffman.getCodes(),lowercase);
		System.out.printf("New length of the string after compression: %s\n", encoded.length());
	    //Height of the tree.
		System.out.printf("Height of the tree is %s", huffman.getHeight(root));
		
	}

}
