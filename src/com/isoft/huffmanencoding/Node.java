package com.isoft.huffmanencoding;

public class Node implements Comparable<Node>{
	private char character;
	private int frequency;
	private Node left, right;
	
	
	public Node(char character, int frequency, Node left, Node right) {
		this.character = character;
		this.frequency = frequency;
		this.left = left;
		this.right = right;
	}
	
	public Node(char character, int frequency) {
		this.character = character;
		this.frequency = frequency;
	}
	
	public char getCharacter() {
		return character;
	}

	public int getFrequency() {
		return frequency;
	}

	public Node getLeft() {
		return left;
	}

	public Node getRight() {
		return right;
	}

	boolean isLeaf() {
		return this.left == null && this.right == null;
	}

	
	@Override
	public String toString() {
		return "Node [character=" + character + ", frequency=" + frequency + "]";
	}

	@Override
	public int compareTo(Node o) {
		int frequencyComparison = Integer.compare(this.frequency, o.frequency);
		if(frequencyComparison != 0)
			return frequencyComparison;
		return Integer.compare(this.character, o.character);
	}
}
